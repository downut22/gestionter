package main;

import java.util.ArrayList;
import java.util.Random;

public class Groupe {

	private int id;
	private ArrayList<Etudiant> members;
	
	private Sujet sujet;
	
	private Random rand ;
	
	public int size() {return members.size();}
	public Etudiant getMember(int i) {return members.get(i);}
	
	//Groupe aléatoire
	public Groupe(int min, int max) {
	
		members = new ArrayList<Etudiant>();
		rand = new Random();
		    
		int r = rand.nextInt(max-min) + min;
				
		for(int i = 0; i < r; i++) 
		{
			members.add(new Etudiant());
		}
	}
}
