package main;

import java.util.ArrayList;

public class Main {

	private ArrayList<Groupe> groups;
	
	  public static void main (String[] args)
	  {
		    System.out.println("Bonjour !");
		    main = new Main();
		    
		    main.GenerateGroups(6);
		    main.DisplayGroups();
	  }
	  
	  void GenerateGroups(int count)
	  {
		  groups = new ArrayList<Groupe>();
		  
		  while(count > 0)
		  {
			  groups.add(new Groupe(2,5));
			  count--;
		  }
	  }
	  
	  void DisplayGroups()
	  {
		  for(int i = 0; i < groups.size();i++)
		  {
			  System.out.println("Groupe " + String.valueOf(i));
			  System.out.println("- " + String.valueOf(i));
			  for(int i2 = 0; i2 < groups.get(i).size();i2++)
			  {
				  System.out.println("-- " + groups.get(i).getMember(i2).getName());
			  }
		  }
	  }
	  
	  public static Main main;
}
